package com.example.demopostgreliquibase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPostgreLiquibaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPostgreLiquibaseApplication.class, args);
	}

}
